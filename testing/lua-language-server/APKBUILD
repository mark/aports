# Maintainer: psykose <alice@ayaya.dev>
pkgname=lua-language-server
pkgver=3.6.3
pkgrel=0
pkgdesc="Language Server for Lua"
url="https://github.com/sumneko/lua-language-server"
arch="all !s390x !ppc64le" # ftbfs
license="MIT"
makedepends="bash samurai"
source="https://github.com/sumneko/lua-language-server/archive/refs/tags/$pkgver/lua-language-server-$pkgver.tar.gz
	lua-language-server-submodules-$pkgver.zip.noauto::https://github.com/sumneko/lua-language-server/releases/download/$pkgver/lua-language-server-$pkgver-submodules.zip
	wrapper
	"
options="!check" # no tests

prepare() {
	default_prepare

	unzip -o "$srcdir"/lua-language-server-submodules-$pkgver.zip.noauto \
		-d "$builddir"
}

build() {
	ninja -C 3rd/luamake -f compile/ninja/linux.ninja
	./3rd/luamake/luamake rebuild
}

package() {
	install -Dm755 "$srcdir"/wrapper "$pkgdir"/usr/bin/lua-language-server
	install -Dm755 bin/lua-language-server \
		-t "$pkgdir"/usr/lib/lua-language-server/bin
	install -Dm644 bin/main.lua \
		-t "$pkgdir"/usr/lib/lua-language-server/bin
	install -Dm644 debugger.lua main.lua \
		-t "$pkgdir"/usr/lib/lua-language-server
	cp -a locale meta script "$pkgdir"/usr/lib/lua-language-server
}

sha512sums="
10f7b23b24be28fe458a4e4da2bbf09aa891904c8019ab08e04c9516ef18e64c537fc50f89b109076abcf6b0842ec9f2b4616775aa1897a6fb992b655f02c2d0  lua-language-server-3.6.3.tar.gz
e4f41f39b1ee2d3f2d347811b54a6441a9edf6c2921e71e6982362b9cc37756157a2ec27cbfcfb9c75a31257fd6a4f64cf1a7be68ba2ee3e5239a5e26474b533  lua-language-server-submodules-3.6.3.zip.noauto
75a65e2e084b1f8e11b88f874ad399f51dbd280c02eaa0d8aa79e7c1fdc9e734104ef4f418f733b8d4df5eadfee8683087cc3d13e783e6104c4e7ffa4671cdf3  wrapper
"
