# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kinit
pkgver=5.100.0
pkgrel=0
pkgdesc="Process launcher to speed up launching KDE applications"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-only OR LGPL-3.0-only"
depends_dev="
	kconfig-dev
	kcrash-dev
	ki18n-dev
	kio-dev
	kservice-dev
	kwindowsystem-dev
	libcap-dev
	qt5-qtbase-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	kdoctools-dev
	samurai
	"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kinit-$pkgver.tar.xz"
options="!check suid" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
092d7278a98693d60d1db31e1efad1d8f1e400fcb6953c63adeb8d2ecf79c4816fc5bdde825388890c6f58f97ad200ffadc9dad6a80357efd3a2e4fd4f97f78a  kinit-5.100.0.tar.xz
"
