# Contributor: Fabian Affolter <fabian@affolter-engineering.ch>
# Maintainer: Fabian Affolter <fabian@affolter-engineering.ch>
pkgname=ansible-lint
pkgver=6.9.0
pkgrel=0
pkgdesc="check ansible playbooks"
url="https://github.com/ansible/ansible-lint"
arch="noarch"
options="!check"
license="MIT"
depends="
	ansible-core
	py3-ansible-compat
	py3-filelock
	py3-jsonschema
	py3-packaging
	py3-rich
	py3-ruamel.yaml
	py3-tenacity
	py3-typing-extensions
	py3-wcmatch
	py3-yaml
	python3
	yamllint
	"
makedepends="
	py3-gpep517
	py3-installer
	py3-setuptools
	py3-setuptools_scm
	py3-wheel
	"
checkdepends="
	py3-flaky
	py3-psutil
	py3-pytest
	py3-pytest-cov
	py3-pytest-xdist
	yamllint
	"
source="ansible-lint-$pkgver-2.tar.gz::https://github.com/ansible-community/ansible-lint/archive/refs/tags/v$pkgver/ansible-lint-v$pkgver.tar.gz"
provides="py3-ansible-lint=$pkgver-r$pkgrel" # for backward compatibility
replaces="py3-ansible-lint" # for backward compatibility

build() {
	export SETUPTOOLS_SCM_PRETEND_VERSION=$pkgver
	gpep517 build-wheel \
		--wheel-dir dist \
		--output-fd 1
}

check() {
	pytest
}

package() {
	python3 -m installer -d "$pkgdir" \
		dist/ansible_lint-$pkgver-py3-none-any.whl
}

sha512sums="
3616a1c18dbf2fa0d20d41087361125e0275b24567014f0a91a5fca1bc26a6f6a4f5ec1f2c65a771418e818b5e66499b7929f9be1dd7c3b07ff52b0153943a3f  ansible-lint-6.9.0-2.tar.gz
"
